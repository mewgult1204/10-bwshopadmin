系统管理
   地 址:  列表 -- area/list   get
           增删改查 -- area/list?  post  get  put  delete
   管理员:  数据 -- sys/user/page   get
           新增 -- role/list?  post
           编辑 -- user/info?（数据回显）   --/role/list ?   get  put
           删除 -- sys/user?   delete
   角 色:  数据 -- role/page?  get
           新增 -- sys/role  post
           编辑 -- sys/menu/table?   -- sys/role/info/？ 回显  get  put
           删除 -- role  delete

   菜 单:  数据 -- /sys/menu/table  get
           新增 -- /sys/menu  post 
           编辑 -- /sys/menu put
           删除 --  /sys/menu/{menuId} delete
           