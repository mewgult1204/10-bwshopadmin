export default [
    {
        path: '/home',
        component: '@/pages/home/index',
        name: '首页'
    },
    {
        path: '/login',
        component: '@/pages/login/index',
        // 不展示顶栏
        headerRender: false,
        // 不展示页脚
        footerRender: false,
        // 不展示菜单
        menuRender: false,
        // 不展示菜单顶栏
        menuHeaderRender: false,
        // 权限配置，需要与 plugin-access 插件配合使用
        access: 'canRead',
        // 隐藏子菜单
        hideChildrenInMenu: false,
        // 隐藏自己和子菜单
        hideInMenu: false,
        // 在面包屑中隐藏
        hideInBreadcrumb: false,
    }
]